# Godot Step by Step

This project contains the work from all the group members who have gone
through Godot's online tutorial. Each folder is organized by every individual 
section that makes part of the entire tutorial. Each member will have
their own branch so they may compare their work with the rest of the group.

You can view Godot's online step by step tutorial 
[here](https://docs.godotengine.org/en/stable/getting_started/step_by_step/index.html)
